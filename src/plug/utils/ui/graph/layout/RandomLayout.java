package plug.utils.ui.graph.layout;

import plug.utils.function.TriConsumer;

import java.util.Collection;
import java.util.Objects;
import java.util.Random;
import java.util.function.Supplier;

/**
 * Created by Ciprian TEODOROV on 20/02/17.
 */
public class RandomLayout<T> {
    public Supplier<Collection<T>> allVerticesGetter;
    public TriConsumer<T, Double, Double> positionSetter;

    public void layout() {
        allVerticesGetter = Objects.requireNonNull(allVerticesGetter);
        positionSetter = Objects.requireNonNull(positionSetter);

        Random rnd = new Random();

        for (T vertex : allVerticesGetter.get()) {

            double x = rnd.nextDouble() * 500;
            double y = rnd.nextDouble() * 500;

            positionSetter.accept(vertex, x, y);

        }
    }
}
