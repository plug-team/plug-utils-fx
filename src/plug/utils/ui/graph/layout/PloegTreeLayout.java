package plug.utils.ui.graph.layout;

import plug.utils.function.TriConsumer;

import java.util.*;
import java.util.function.Function;

/**
 * Created by Ciprian TEODOROV on 21/02/17.
 *
 * This layout is an adaptation of the extended Reingold-Tilford algorithm as described in the paper
 * "Drawing Non-layered Tidy Trees in Linear Time" by Atze van der Ploeg
 * Accepted for publication in Software: Practice and Experience, to Appear.
 *
 * The original implementation can be found at:
 * @see <a href="https://github.com/cwi-swat/non-layered-tidy-trees">non-layered-tidy-trees</a>
 *
 * This code is in the public domain, use it any way you wish. A reference to the paper is
 * appreciated!
 */

public class PloegTreeLayout<T> {
    @FunctionalInterface
    public interface IFanoutGetter<T> {
        List<T> get(T node);
    }

    double defaultWidth = 5.0;
    double defaultHeight = 5.0;
    public double defaultLevelSeparation = 20.0;

    //if the layout processing is too slow the results of the following getters could be cached
    public IFanoutGetter<T> fanoutGetter;
    public Function<T, Double> widthGetter;
    public Function<T, Double> heightGetter;

    public TriConsumer<T, Double, Double> positionSetter;

    private class LayoutData<T> {
        double x; //the x position of a node
        double y; //the y position of a node
        double prelim, mod, shift, change;
        T leftThread, rightThread;          // ^{\normalfont Left and right thread.}^
        T extremeLeft, extremeRight;          // ^{\normalfont Extreme left and right nodes.}^
        double msel, mser;    // ^{\normalfont Sum of modifiers at the extreme nodes.}^
    }

    Map<T, LayoutData<T>> layoutDataMap = new IdentityHashMap<>();

    T virtualRoot;
    Collection<T> roots;
    public void layout(List<T> initialVertices, T virtualRoot) {
        this.virtualRoot = virtualRoot;
        roots = initialVertices;
        layout(virtualRoot);
    }

    public void layout(T root) {
        firstWalk(root, 0);
        secondWalk(root, 0);
    }

    void firstWalk(T node, double yPosition) {
        ensureLayoutData(node, yPosition);
        List<T> children = getChildren(node);
        int numberOfChildren = children.size();
        if (numberOfChildren == 0) {
            setExtremes(node);
            return;
        }
        T leftmostChild = children.get(0);
        //children of node are at node.y + node.height
        double childrenY = yPosition + getHeight(node) + defaultLevelSeparation;
        firstWalk(leftmostChild, childrenY);
        // ^{\normalfont Create siblings in contour minimal vertical coordinate and index list.}^
        IYL ih = updateIYL(bottom(getExtremeLeft(leftmostChild)), 0, null);
        for (int i = 1; i < numberOfChildren; i++) {
            T ithChild = children.get(i);
            firstWalk(ithChild, childrenY);
            //^{\normalfont Store lowest vertical coordinate while extreme nodes still point in current subtree.}^
            double minY = bottom(getExtremeRight(ithChild));
            seperate(node, i, ih);
            ih = updateIYL(minY, i, ih);

        }
        positionRoot(node);
        setExtremes(node);
    }

    void ensureLayoutData(T node, double yPosition) {
        //create the layoutData for the node
        if (!layoutDataMap.containsKey(node)) {
            LayoutData<T> data = new LayoutData<T>();
            layoutDataMap.put(node, data);

            data.y = yPosition;
        }
    }

    public void setDefaultWidth(double defaultWidth) {
        this.defaultWidth = defaultWidth;
    }

    public void setDefaultHeight(double defaultHeight) {
        this.defaultHeight = defaultHeight;
    }

    T getExtremeLeft(T node) {
        return layoutDataMap.get(node).extremeLeft;
    }

    T getExtremeRight(T node) {
        return layoutDataMap.get(node).extremeRight;
    }

    T getLeftThread(T node) {
        return layoutDataMap.get(node).leftThread;
    }

    T getRightThread(T node) {
        return layoutDataMap.get(node).rightThread;
    }

    double getPrelim(T node) {
        return layoutDataMap.get(node).prelim;
    }

    void setPrelim(T node, double prelim) {
        layoutDataMap.get(node).prelim = prelim;
    }

    double getMod(T node) {
        return layoutDataMap.get(node).mod;
    }

    double getMsel(T node) {
        return layoutDataMap.get(node).msel;
    }

    double getMser(T node) {
        return layoutDataMap.get(node).mser;
    }

    double getWidth(T node) {
        if (node == virtualRoot) return 0;
        if (widthGetter == null) {
            return defaultWidth;
        }
        Double width = widthGetter.apply(node);
        return width == null ? defaultWidth : width;
    }

    double getHeight(T node) {
        if (node == virtualRoot) return 0;
        if (heightGetter == null) {
            return defaultHeight;
        }
        Double height = heightGetter.apply(node);
        return height == null ? defaultHeight : height;
    }

    double getY(T node) {
        return layoutDataMap.get(node).y;
    }

    List<T> getChildren(T node) {
        if (node == virtualRoot) {
            return roots == null ? Collections.emptyList() : new ArrayList<>(roots);
        }
        List<T> children = fanoutGetter.get(node);
        return children == null ? Collections.emptyList() : children;
    }

    void updatePosition(T node, double x, double y) {
        if (node == virtualRoot) {
            return;
        }
        positionSetter.accept(node, x, y);
    }

    double bottom(T node) {
        return getY(node) + getHeight(node);
    }

    void positionRoot(T node) {
        // ^{\normalfont Position root between children, taking into account their mod.}^
        List<T> children = getChildren(node);
        T leftmostChild = children.get(0);
        T rightmostChild = children.get(children.size() - 1);

        double prelim = (getPrelim(leftmostChild) +
                getMod(leftmostChild) +
                getPrelim(rightmostChild) +
                getMod(rightmostChild) +
                getWidth(rightmostChild)) / 2 - getWidth(node) / 2;

        setPrelim(node, prelim);
    }

    void setExtremes(T node) {
        List<T> children = getChildren(node);

        LayoutData<T> data = layoutDataMap.get(node);
        if (children.size() == 0) {
            //if this node is a leaf its extremes are itself
            data.extremeLeft = node;
            data.extremeRight = node;
            data.msel = data.mser = 0;
            return;
        }
        T leftmostChild = children.get(0);
        T rightmostChild = children.get(children.size() - 1);
        //the extreme left is the extreme left of its leftmost child
        data.extremeLeft = getExtremeLeft(leftmostChild);
        data.msel = getMsel(leftmostChild);
        //the extreme right is the extreme right of its rightmost child
        data.extremeRight = getExtremeRight(rightmostChild);
        data.mser = getMser(rightmostChild);
    }

    void seperate(T node, int i, IYL ih) {
        List<T> children = getChildren(node);
        // ^{\normalfont Right contour node of left siblings and its sum of modfiers.}^
        T sr = children.get(i - 1);
        double mssr = getMod(sr);
        // ^{\normalfont Left contour node of current subtree and its sum of modfiers.}^
        T cl = children.get(i);
        double mscl = getMod(cl);
        while (sr != null && cl != null) {
            if (bottom(sr) > ih.lowY) ih = ih.nxt;
            // ^{\normalfont How far to the left of the right side of sr is the left side of cl?}^
            double dist = (mssr + getPrelim(sr) + getWidth(sr)) - (mscl + getPrelim(cl));
            if (dist > 0) {
                mscl += dist;
                moveSubtree(node, i, ih.index, dist);
            }
            double sy = bottom(sr), cy = bottom(cl);
            // ^{\normalfont Advance highest node(s) and sum(s) of modifiers}^
            if (sy <= cy) {
                sr = nextRightContour(sr);
                if (sr != null) mssr += getMod(sr);
            }
            if (sy >= cy) {
                cl = nextLeftContour(cl);
                if (cl != null) mscl += getMod(cl);
            }
        }
        // ^{\normalfont Set threads and update extreme nodes.}^
        // ^{\normalfont In the first case, the current subtree must be taller than the left siblings.}^
        if (sr == null && cl != null) setLeftThread(node, i, cl, mscl);
            // ^{\normalfont In this case, the left siblings must be taller than the current subtree.}^
        else if (sr != null && cl == null) setRightThread(node, i, sr, mssr);
    }

    void moveSubtree(T node, int i, int si, double dist) {
        List<T> children = getChildren(node);
        // ^{\normalfont Move subtree by changing mod.}^
        LayoutData<T> data = layoutDataMap.get(children.get(i));
        data.mod += dist;
        data.msel += dist;
        data.mser += dist;
        distributeExtra(node, i, si, dist);
    }

    T nextLeftContour(T node) {
        List<T> children = getChildren(node);

        return children.size() == 0 ? getLeftThread(node) : children.get(0);
    }

    T nextRightContour(T node) {
        List<T> children = getChildren(node);

        return children.size() == 0 ? getRightThread(node) : children.get(children.size() - 1);
    }

    void distributeExtra(T node, int i, int si, double dist) {
        // ^{\normalfont Are there intermediate children?}^
        if (si != i - 1) {
            List<T> children = getChildren(node);
            double nr = i - si;
            LayoutData<T> si1Data = layoutDataMap.get(children.get(si + 1));
            si1Data.shift += dist / nr;
            LayoutData<T> ithData = layoutDataMap.get(children.get(i));
            ithData.shift -= dist / nr;
            ithData.change -= dist - dist / nr;
        }
    }

    void setLeftThread(T node, int i, T cl, double modsumcl) {
        List<T> children = getChildren(node);
        LayoutData<T> leftmostData = layoutDataMap.get(children.get(0));
        LayoutData<T> ithData = layoutDataMap.get(children.get(i));
        LayoutData<T> clData = layoutDataMap.get(cl);
        LayoutData<T> liData = layoutDataMap.get(leftmostData.extremeLeft);

        liData.leftThread = cl;
        // ^{\normalfont Change mod so that the sum of modifier after following thread is correct.}^
        double diff = (modsumcl - clData.mod) - leftmostData.msel;
        liData.mod += diff;
        // ^{\normalfont Change preliminary x coordinate so that the node does not move.}^
        liData.prelim -= diff;
        // ^{\normalfont Update extreme node and its sum of modifiers.}^
        leftmostData.extremeLeft = ithData.extremeLeft;
        leftmostData.msel = ithData.msel;
    }

    // ^{\normalfont Symmetrical to setLeftThread.}^
    void setRightThread(T node, int i, T sr, double modsumsr) {
        List<T> children = getChildren(node);

        LayoutData<T> beforeIData = layoutDataMap.get(children.get(i - 1));
        LayoutData<T> ithData = layoutDataMap.get(children.get(i));
        LayoutData<T> srData = layoutDataMap.get(sr);
        LayoutData<T> riData = layoutDataMap.get(ithData.extremeRight);
        riData.rightThread = sr;


        double diff = (modsumsr - srData.mod) - ithData.mser;
        riData.mod += diff;
        riData.prelim -= diff;
        ithData.extremeRight = beforeIData.extremeRight;
        ithData.mser = beforeIData.mser;
    }

    void secondWalk(T node, double modsum) {
        List<T> children = getChildren(node);
        LayoutData<T> nodeData = layoutDataMap.get(node);
        modsum += nodeData.mod;
        // ^{\normalfont Set absolute (non-relative) horizontal coordinate.}^
        nodeData.x = nodeData.prelim + modsum;
        updatePosition(node, nodeData.x, nodeData.y);
        addChildSpacing(node);
        for (int i = 0; i < children.size(); i++) {
            secondWalk(children.get(i), modsum);
        }
    }

    // ^{\normalfont Process change and shift to add intermediate spacing to mod.}^
    void addChildSpacing(T node) {
        List<T> children = getChildren(node);
        double d = 0, modsumdelta = 0;
        for (int i = 0; i < children.size(); i++) {
            LayoutData ithData = layoutDataMap.get(children.get(i));
            d += ithData.shift;
            modsumdelta += d + ithData.change;
            ithData.mod += modsumdelta;
        }
    }

    // ^{\normalfont A linked list of the indexes of left siblings and their lowest vertical coordinate.}^
    static class IYL {
        double lowY;
        int index;
        IYL nxt;

        public IYL(double lowY, int index, IYL nxt) {
            this.lowY = lowY;
            this.index = index;
            this.nxt = nxt;
        }
    }

    static IYL updateIYL(double minY, int i, IYL ih) {
        // ^{\normalfont Remove siblings that are hidden by the new subtree.}^
        while (ih != null && minY >= ih.lowY) ih = ih.nxt;
        // ^{\normalfont Prepend the new subtree.}^
        return new IYL(minY, i, ih);
    }
}
