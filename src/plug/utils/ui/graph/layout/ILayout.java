package plug.utils.ui.graph.layout;

import java.util.function.Consumer;

/**
 * Created by Ciprian TEODOROV on 20/02/17.
 */
@FunctionalInterface
public interface ILayout extends Consumer {}
