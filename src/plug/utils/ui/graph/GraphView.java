package plug.utils.ui.graph;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.ObservableSet;
import javafx.collections.SetChangeListener;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Group;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ZoomEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import plug.utils.Pair;
import plug.utils.ui.graph.GraphViewModel.Edge;
import plug.utils.ui.graph.layout.PloegTreeLayout;


/**
 * A {@link GraphView} presents a {@link GraphViewModel} in a layouted tree.
 */
public class GraphView<V, E> extends ScrollPane {

    protected double defaultLevelSeparation = 20;
    protected double defaultNodeSeparation = 30;

    protected double relocateAnimationTiming = 400;

    protected BooleanProperty hideAllDetails = new SimpleBooleanProperty(false);
    protected BooleanProperty coloredNodes = new SimpleBooleanProperty(true);

    protected DoubleProperty nodeMaxWidth = new SimpleDoubleProperty(400);
    protected DoubleProperty nodeMaxHeight = new SimpleDoubleProperty(200);

    protected final Group group = new Group();
    protected final Pane pane = new Pane();
    protected final Group nodeGroup = new Group();
    protected final Group edgeGroup = new Group();

    protected final SimpleObjectProperty<V> selectedVertex = new SimpleObjectProperty<>(null);

    protected final ObjectProperty<GraphViewModel<V, E>> model = new SimpleObjectProperty<>();

    private final ListChangeListener<V> onInitialVerticesChanged = this::onInitialVerticesChanged;
    private final ListChangeListener<Edge<V,E>> onEdgesChanged = this::onEdgesChanged;

    protected final ObservableList<VertexView<V,E>> initialVertices = FXCollections.observableArrayList();
    protected final ObservableMap<V, VertexView<V,E>> vertexViews = FXCollections.observableMap(new IdentityHashMap<>());
    protected final ObservableMap<Edge<V,E>, EdgeView<V,E>> edgeViews = FXCollections.observableMap(new IdentityHashMap<>());

    //TODO: 3 - When adding an edge scroll to focus on that edge

    /** Timeline used to call layout once every 250ms at most */
    protected final Timeline layoutTimeline = new Timeline(
            new KeyFrame(Duration.millis(100), (event) -> ploegTreeLayout())
    );

    public GraphView() {
        super();
        //add the edgeViews and nodes to the pane
        pane.getChildren().addAll(edgeGroup, nodeGroup);

        //create a group containing the Pane to make the scroll work with zoom
        group.getChildren().add(pane);

        //add the group to the scrollpane
        this.setContent(group);

        //register the zoom handler on the ScrollPane this way we can zoom even when the pane is very small
        this.addEventHandler(ZoomEvent.ZOOM, e -> {
            zoom(e.getZoomFactor());
            //consume the zoom event
            e.consume();
        });

        //set myself pane-able
        this.setPannable(true);

        model.addListener((observable, oldValue, newValue) -> {
            if (oldValue != null) {
                oldValue.getInitialVertices().removeListener(onInitialVerticesChanged);
                oldValue.getEdges().removeListener(onEdgesChanged);
            }
            initialVertices.clear();
            vertexViews.clear();
            edgeViews.clear();
            if (newValue != null) {
                newValue.getInitialVertices().addListener(onInitialVerticesChanged);
                newValue.getEdges().addListener(onEdgesChanged);

                for (V initial : newValue.getInitialVertices()) {
                    initialVertices.add(getOrCreateVertex(initial));
                }

                for (Edge<V, E> edge : newValue.getEdges()) {
                    getOrCreateEdgeView(edge);
                }
            }
        });

        selectedVertex.addListener(this::selectionChanged);
        foldedNodes.addListener(this::foldedNodesUpdate);

        vertexViews.addListener((MapChangeListener<V,VertexView<V,E>>) change -> {
        	if (change.getValueAdded() != null) {
                nodeGroup.getChildren().add(change.getValueAdded());
            }
            if (change.getValueRemoved() != null) {
        	    nodeGroup.getChildren().remove(change.getValueRemoved());
            }
            refreshTreeLayout();
        });

        edgeViews.addListener((MapChangeListener<Edge<V,E>, EdgeView<V,E>>) change -> {
        	if (change.getValueAdded() != null) {
                edgeGroup.getChildren().add(change.getValueAdded());
            }
            if (change.getValueRemoved() != null) {
        	    edgeGroup.getChildren().remove(change.getValueRemoved());
            }
            refreshTreeLayout();
        });

        isVertical.addListener((a,o,n) -> refreshTreeLayout());
    }

    public ObjectProperty<GraphViewModel<V, E>> modelProperty() {
        return model;
    }

    public GraphViewModel<V, E> getModel() {
        return model.get();
    }

    public void setModel(GraphViewModel<V, E> model) {
        this.model.set(model);
    }

    public SimpleObjectProperty<V> selectedVertexProperty() {
        return selectedVertex;
    }

    public V getSelectedVertex() {
        return selectedVertex.get();
    }

    public void setSelectedVertex(V selectedVertex) {
        this.selectedVertex.set(selectedVertex);
    }

    public DoubleProperty nodeMaxWidthProperty() {
        return nodeMaxWidth;
    }

    public double getNodeMaxWidth() {
        return nodeMaxWidth.get();
    }

    public void setNodeMaxWidth(double nodeMaxWidth) {
        this.nodeMaxWidth.set(nodeMaxWidth);
    }

    public DoubleProperty nodeMaxHeightProperty() {
        return nodeMaxHeight;
    }

    public double getNodeMaxHeight() {
        return nodeMaxHeight.get();
    }

    public void setNodeMaxHeight(double nodeMaxHeight) {
        this.nodeMaxHeight.set(nodeMaxHeight);
    }

    public BooleanProperty hideAllDetailsProperty() {
        return hideAllDetails;
    }

    public BooleanProperty coloredNodesProperty() {
        return coloredNodes;
    }

    public ObservableSet<VertexView> getFoldedNodes() {
        return foldedNodes;
    }

    public void scrollToVertexView(VertexView<V,E> vertex) {
        double width = this.getContent().getBoundsInLocal().getWidth();
        double height = this.getContent().getBoundsInLocal().getHeight();

        double x = vertex.getBoundsInParent().getMaxX();
        double y = vertex.getBoundsInParent().getMaxY();

        // scrolling values range from 0 to 1
        this.setVvalue(y/height);
        this.setHvalue(x/width);
    }

    public void actualSize() {
        zoom(1.0/pane.getScaleX());
        pane.getParent().setTranslateX(0);
        pane.getParent().setTranslateY(0);
    }

    public void zoomToFit() {
        double widthFactor = this.getWidth() / pane.getWidth();
        double heightFactor = this.getHeight() / pane.getHeight();

        double zoomFactor = widthFactor < heightFactor ? widthFactor : heightFactor;

        zoom(zoomFactor/pane.getScaleX());
    }

    public void zoom(double zoomFactor) {
        //scale the Pane
        pane.setScaleX(pane.getScaleX() * zoomFactor);
        pane.setScaleY(pane.getScaleY() * zoomFactor);
    }

    public void refreshTreeLayout() {
        layoutTimeline.playFromStart();
    }

    //create a node for the given vertex
    public VertexView<V,E> buildNodeForVertex(V vertex) {
        VertexView<V,E> vertexView = new VertexView<>(this, vertex);
        vertexView.hideDetailsProperty().bind(hideAllDetails);
        vertexView.coloredProperty().bind(coloredNodes);
        return vertexView;
    }

    BooleanProperty isVertical = new SimpleBooleanProperty(true);

    public BooleanProperty isVerticalProperty() {
        return isVertical;
    }
    public void beVertical() {
        isVertical.set(true);
    }
    public void beHorizontal() {
        isVertical.set(false);
    }

    @SuppressWarnings("unchecked")
    protected void foldedNodesUpdate(SetChangeListener.Change<? extends VertexView> change) {

        VertexView<V,E> added = change.getElementAdded();
        if(added != null) {
            //compute the reachable set and remove it from view
			Pair<Set<VertexView<V,E>>, Set<EdgeView<V,E>>> reachable = reachableGraphicalElementsFrom(added);
            nodeGroup.getChildren().removeAll(reachable.a);
            edgeGroup.getChildren().removeAll(reachable.b);
        }

        VertexView<V,E> removed = change.getElementRemoved();
        if (removed != null) {
            //compute the reachable set and add it to the view
			Pair<Set<VertexView<V,E>>, Set<EdgeView<V,E>>> reachable = reachableGraphicalElementsFrom(removed);
            nodeGroup.getChildren().addAll(reachable.a);
            edgeGroup.getChildren().addAll(reachable.b);
        }

        refreshTreeLayout();
    }

    protected void ploegTreeLayout() {
        PloegTreeLayout<VertexView<V,E>> layout = new PloegTreeLayout<>();

        layout.fanoutGetter = this::foldedFanoutGetter;

        if (isVertical.get()) {
            layout.positionSetter = this::relocateVertical;
            layout.widthGetter = v -> v.getLayoutBounds().getWidth() + defaultNodeSeparation;
            layout.heightGetter = v -> v.getLayoutBounds().getHeight();
        } else {
            layout.positionSetter = this::relocateHorizontal;
            layout.widthGetter = v -> v.getLayoutBounds().getHeight() + defaultNodeSeparation;
            layout.heightGetter = v -> v.getLayoutBounds().getWidth();
        }
        layout.defaultLevelSeparation = defaultLevelSeparation;

        if (initialVertices.size() > 1) {
            layout.layout(initialVertices, new VertexView<>(null, null));
        } else {
            layout.layout(initialVertices.stream().findFirst().orElse(null));
        }
    }

    //this is the callback if horizontal layout is wanted
    private void relocateHorizontal(VertexView node, double x, double y) {
        relocateNode(node, y, x);
    }

    //this is the callback if vertical layout is wanted
    private void relocateVertical(VertexView node, double x, double y) {
        relocateNode(node, x, y);
    }

    //this method is passed to the layout to relocate the nodes
    private void relocateNode(VertexView node, double x, double y) {
        // if there is too much vertices, don't do the animation
        if (vertexViews.size() < 500) {
            final Timeline timeline = new Timeline();
            timeline.setCycleCount(1);
            final KeyValue kv0 = new KeyValue(node.layoutXProperty(), x);
            final KeyValue kv1 = new KeyValue(node.layoutYProperty(), y);
            final KeyFrame kf0 = new KeyFrame(Duration.millis(relocateAnimationTiming), kv0);
            final KeyFrame kf1 = new KeyFrame(Duration.millis(relocateAnimationTiming), kv1);
            timeline.getKeyFrames().add(kf0);
            timeline.getKeyFrames().add(kf1);
            timeline.play();
        } else {
            node.layoutXProperty().set(x);
            node.layoutYProperty().set(y);
        }
        //v.pane.relocate(x, y);
    }

    protected final SimpleBooleanProperty nodeFolding = new SimpleBooleanProperty(true);

    public BooleanProperty nodeFoldingProperty() {
        return nodeFolding;
    }

    public void setNodeFolding(boolean nodeFolding) {
        this.nodeFolding.set(nodeFolding);
    }

    protected final ObservableSet<VertexView> foldedNodes = FXCollections.observableSet();

    //compute the reachable graphical elements from a vertex
    protected Pair<Set<VertexView<V,E>>, Set<EdgeView<V,E>>> reachableGraphicalElementsFrom(VertexView<V,E> v) {
        Queue<VertexView<V, E>> toSee = new LinkedList<>();
        Set<VertexView<V, E>> known = Collections.newSetFromMap(new IdentityHashMap<>());

        Pair<Set<VertexView<V,E>>, Set<EdgeView<V, E>>> result = new Pair<>(Collections.newSetFromMap(new IdentityHashMap<>()), Collections.newSetFromMap(new IdentityHashMap<>()));

        toSee.add(v);

        while (!toSee.isEmpty()) {
            VertexView<V, E> source = toSee.remove();

            for (EdgeView<V, E> edge : edgeViewsFrom(source)) {
                if (known.add(edge.getTarget())) {
                    if (!foldedNodes.contains(edge.getTarget())) {
                        toSee.add(edge.getTarget());
                    }
                    result.a.add(edge.getTarget());
                }
                result.b.add(edge);
            }
        }
        return result;
    }

    protected List<VertexView<V,E>> foldedFanoutGetter(VertexView<V,E> v) {
        if (foldedNodes.contains(v)) return Collections.emptyList();
        return edgeViewsFrom(v).stream().map(e -> e.target).collect(Collectors.toList());
    }

    protected void onInitialVerticesChanged(ListChangeListener.Change<? extends V> change) {
        Platform.runLater(() -> {
            change.reset();
            while (change.next()) {
                for (V toAdd : change.getAddedSubList()) {
                    initialVertices.add(getOrCreateVertex(toAdd));
                }
                for (V toRemove : change.getRemoved()) {
                    VertexView<V,E> vertex = removeVertexIfExists(toRemove);
                    initialVertices.remove(vertex);
                }
            }
        });
    }

    protected void onEdgesChanged(ListChangeListener.Change<? extends Edge<V,E>> change) {
        Platform.runLater(() -> {
            change.reset();
            while (change.next()) {
                for (Edge<V,E> toRemove : change.getRemoved()) {
                    removeEdgeViewIfExists(toRemove);
                }

                for (Edge<V,E> toAdd : change.getAddedSubList()) {
                    getOrCreateEdgeView(toAdd);
                }
            }

            updateVerticesColor();
        });
    }

    protected VertexView<V,E> getOrCreateVertex(V toAdd) {
        return vertexViews.computeIfAbsent(toAdd, (a) -> buildNodeForVertex(toAdd));
    }

    protected VertexView<V,E> removeVertexIfExists(V toRemove) {
        VertexView<V,E> vertex = vertexViews.remove(toRemove);
        if (vertex != null) {
            nodeGroup.getChildren().remove(vertex);
            if (selectedVertexProperty().get() == toRemove) {
                selectedVertexProperty().set(null);
            }
        }
        return vertex;
    }

    protected EdgeView<V,E> getOrCreateEdgeView(Edge<V,E> edge) {
        VertexView<V,E> sourceView = getOrCreateVertex(edge.source);
        VertexView<V,E> targetView = getOrCreateVertex(edge.target);

        EdgeView<V,E> edgeView = new EdgeView<>(this, edge);
        edgeViews.put(edge, edgeView);
        return edgeView;
    }

    protected EdgeView<V, E> removeEdgeViewIfExists(Edge<? extends V, ? extends E> edge) {
        removeVertexIfExists(edge.target);
        return edgeViews.remove(edge);
    }

    public List<EdgeView<V, E>> edgeViewsFrom(VertexView<V,E> source) {
        return edgeViews.entrySet().stream().filter(e -> e.getKey().source == source.getData()).map(e -> e.getValue()).collect(Collectors.toList());
    }

    protected void updateVerticesColor() {
        GraphViewModel<V, E> model = this.model.get();
        for (VertexView<V, E> vertexView : vertexViews.values()) {
            vertexView.setColor(model.vertexColor(vertexView.getData()));
        }
    }

    private void selectionChanged(ObservableValue<? extends V> observable, V oldValue, V newValue) {
        Platform.runLater(() -> {
            if (oldValue != null) {
                // un-select equivalent
                for (V equivalent : model.get().equivalentVertices(oldValue)) {
                    VertexView<V, E> view = vertexViews.get(equivalent);
                    if (view != null) view.selectedProperty().set(false);
                }
                if (vertexViews.containsKey(oldValue)) {
                    vertexViews.get(oldValue).selectedProperty().set(false);
                }
            }
            if (newValue != null) {
                // select equivalent
                for (V equivalent : model.get().equivalentVertices(newValue)) {
                    vertexViews.get(equivalent).selectedProperty().set(true);
                }

                VertexView<V,E> vertexView = vertexViews.get(newValue);
                if (vertexView != null) {
                    vertexView.selectedProperty().set(true);
                }
            }
        });
    }

    public void savePngImage(File imageFile) throws IOException {
        double previousScaleX = pane.getScaleX();
        double previousScaleY = pane.getScaleY();

        try {
            // uses a 3 scale to create big images always at the same scale
            pane.setScaleX(2);
            pane.setScaleY(2);

            SnapshotParameters params = new SnapshotParameters();
            params.setFill(Color.valueOf("#f1f1f1"));
            WritableImage image = pane.snapshot(params, null);
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", imageFile);
        } finally {
            pane.setScaleX(previousScaleX);
            pane.setScaleY(previousScaleY);
        }
    }
}
