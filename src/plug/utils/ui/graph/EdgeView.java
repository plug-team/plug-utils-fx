package plug.utils.ui.graph;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import plug.utils.ui.graph.GraphViewModel.Edge;

/**
 * Created by Ciprian TEODOROV on 20/02/17.
 */
public class EdgeView<V, E> extends Group {

    protected final GraphView<V, E> parent;

    protected final Edge<V, E> data;

    protected final VertexView<V, E> source;
    protected final VertexView<V, E> target;

    protected final Line line;

    public EdgeView(GraphView<V, E> parent, Edge<V, E> data) {
        this.parent = parent;
        this.data = data;
        this.source = parent.vertexViews.get(data.source);
        this.target = parent.vertexViews.get(data.target);

        line = new Line();
        line.setStrokeWidth(5);
        line.setStroke(Color.GRAY);

        line.startXProperty().bind( source.layoutXProperty().add(source.widthProperty().divide(2.0)));
        line.startYProperty().bind( source.layoutYProperty().add(source.heightProperty().divide(2.0)));

        line.endXProperty().bind( target.layoutXProperty().add( target.widthProperty().divide(2.0)));
        line.endYProperty().bind( target.layoutYProperty().add( target.heightProperty().divide(2.0)));

        getChildren().add(line);

        if (data.label != null) {
            String text = parent.getModel().edgeDescription(data.label);
            Tooltip.install(line, new Tooltip(text));
        }

        Platform.runLater(() -> { source.toFront(); target.toFront(); line.toBack(); } );
    }

    public Edge<V, E> getData() {
        return data;
    }

    public VertexView<V, E> getSource() {
        return source;
    }

    public VertexView<V, E> getTarget() {
        return target;
    }
}
