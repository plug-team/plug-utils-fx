package plug.utils.ui.graph;

import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableSet;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.utils.Pair;

/**
 * VertexView for a {@link GraphView}.
 */
public class VertexView<T, E> extends BorderPane {

    protected final GraphView<T, E> parent;

    protected final T data;
    protected final Region node;

    protected final FontIcon foldIcon;
    protected final FontIcon removeIcon;

    protected final BooleanProperty selected = new SimpleBooleanProperty(false);
    protected final BooleanProperty hideDetails = new SimpleBooleanProperty(false);
    protected final BooleanProperty colored = new SimpleBooleanProperty(true);

    protected final ObjectProperty<Color> color = new SimpleObjectProperty<>(Color.WHITE);

    protected Border border = new Border(new BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.SOLID, new CornerRadii(5), new BorderWidths(3)));
    protected Border selectedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(5), new BorderWidths(3)));

    public VertexView(GraphView<T, E> parent, T data) {
        this.parent = parent;
        this.data = data;
        if (parent == null && data == null) {
            //this is a virtual root
            node = null;
            foldIcon = null;
            removeIcon = null;
            return;
        }
        GraphViewModel<T, E> model = parent.getModel();
        this.node = model.vertexNode(data);

        setPadding(new Insets(5));
        setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(5), new Insets(0))));
        setBorder(border);

        BorderPane header = new BorderPane();

        Label id = new Label(model.vertexDescription(data));
        header.setLeft(id);

        boolean sink = model.vertexIsSink(data);
        foldIcon = new FontIcon(sink ? "typ-cancel:20" : "typ-chevron-right:20");

        removeIcon = new FontIcon("typ-delete:20");

        HBox tools = new HBox(removeIcon, foldIcon);
        HBox.setMargin(removeIcon, new Insets(0, 0, 0, 5));
        HBox.setMargin(foldIcon, new Insets(0, 0, 0, 5));
        header.setRight(tools);

        setTop(header);

        removeIcon.setOnMouseClicked(e -> {
            if (e.getClickCount() == 1) {
                Pair<Set<VertexView<T, E>>, Set<EdgeView<T, E>>> pair = parent.reachableGraphicalElementsFrom(this);
                for (EdgeView<T, E> edgeView : pair.b) {
                    model.getEdges().remove(edgeView.getData());
                }
                model.removeVertex(data);
            }
        });

        if (!sink) {
            foldIcon.setOnMouseClicked((e) -> {
                if (e.getClickCount() == 1 && parent.nodeFoldingProperty().get()) {
                    ObservableSet<VertexView> foldedNodes = parent.getFoldedNodes();
                    if (foldedNodes.contains(this)) {
                        foldedNodes.remove(this);
                        foldIcon.setIconLiteral("typ-chevron-right:20");
                    }
                    else {
                        foldedNodes.add(this);
                        foldIcon.setIconLiteral("typ-chevron-left:20");
                    }
                }
            });
        }

        setMaxWidth(parent.getNodeMaxWidth());
        setMaxHeight(parent.getNodeMaxHeight());

        setOnMouseClicked((e) -> parent.setSelectedVertex(this.getData()));

        selectedProperty().addListener((observable, oldValue, newValue) -> {
            setBorder(newValue ? selectedBorder : border);
        });

        if (node != null) {
            setCenter(node);
            header.prefWidthProperty().bind(node.prefWidthProperty());

            hideDetailsProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    setCenter(null);
                    header.prefWidthProperty().unbind();
                    header.prefWidthProperty().set(0);
                } else {
                    setCenter(node);
                    header.prefWidthProperty().bind(node.prefWidthProperty());
                }
            });

            node.setOnMouseClicked((e) -> parent.setSelectedVertex(this.getData()));
        }

        widthProperty().addListener((e) -> parent.refreshTreeLayout());
        heightProperty().addListener((e) -> parent.refreshTreeLayout());

        colored.addListener((observable, oldValue, newValue) -> {
            Color newColor = newValue ? color.get() : Color.WHITE;
            setBackground(new Background(new BackgroundFill(newColor, new CornerRadii(5), new Insets(0))));
        });

        color.addListener((observable, oldValue, newValue) -> {
            Color newColor = newValue != null && colored.get() ? newValue : Color.WHITE;
            setBackground(new Background(new BackgroundFill(newColor, new CornerRadii(5), new Insets(0))));
        });
        color.set(model.vertexColor(data));
    }

    public Region getNode() {
        return this.node;
    }

    public T getData() {
        return data;
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public BooleanProperty hideDetailsProperty() {
        return hideDetails;
    }

    public BooleanProperty coloredProperty() {
        return colored;
    }

    public ObjectProperty<Color> colorProperty() {
        return color;
    }

    public Color getColor() {
        return color.get();
    }

    public void setColor(Color color) {
        this.color.set(color);
    }

    @Override
    public String toString() {
        return String.format("V[%s]",data.toString());
    }
}
