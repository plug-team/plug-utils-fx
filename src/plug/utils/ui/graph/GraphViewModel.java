package plug.utils.ui.graph;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

/**
 * Model for a {@link GraphView}. It contains a list of initial vertices and a
 * list of edges as pairs of source and target.
 *
 * @param <V> vertex type
 */
public class GraphViewModel<V, E> {

	public static class Edge<V, E> {
		public final V source;
		public final V target;

		public final E label;

		public Edge(V source, V target, E label) {
			this.source = source;
			this.target = target;
			this.label = label;
		}
	}

	protected final ObservableList<V> initialVertices = FXCollections.observableArrayList();

	protected final ObservableList<Edge<V, E>> edges = FXCollections.observableArrayList();

	/** List of initial vertices. */
	public ObservableList<V> getInitialVertices() {
		return initialVertices;
	}

	/** List of edges as pairs of source and targets. */
	public ObservableList<Edge<V, E>> getEdges() {
		return edges;
	}

	/** Adds a new pair from source to target. The source must already be a
	 * target of a precedent edge or in initial vertices. */
	public Edge<V, E> addEdge(V source, V target, E label) {
		Edge<V, E> edge = new Edge<>(source, target, label);
		edges.add(edge);
		return edge;
	}

	/**
	 * Removes given vertex from either initial vertices or the edge leading
	 * to it.
 	 */
	public boolean removeVertex(V vertex) {
		// tests in initial vertices
		if (initialVertices.remove(vertex)) {
			return true;
		}

		// test in edges with identity on target data
		int indexToRemove = -1;
		for (int i = 0; i < edges.size(); i++) {
			Edge<V, E> edge = edges.get(i);
			if (edge.target == vertex) {
				indexToRemove = i;
				break;
			}
		}

		if (indexToRemove >= 0) {
			edges.remove(indexToRemove);
		}
		return indexToRemove >= 0;
	}

	/** List of edges from given source. */
	public List<Edge<V, E>> edgesFrom(V source) {
		return edges.stream().filter(e -> e.source == source).collect(Collectors.toList());
	}

	/** Description for the vertex header. */
	public String vertexDescription(V vertex) {
		return vertex.toString();
	}

	public String edgeDescription(E edge) { return edge.toString(); }

	/** The details of the vertex as JavaFX {@link javafx.scene.layout.Region}. */
	public Region vertexNode(V vertex) {
		return null;
	}

	/** Returns true if the vertex is a sink, i.e. it can't have any output
	 * edge. The graph will show a special marker expressing that the vertex is
	 * a sink. */
	public boolean vertexIsSink(V vertex) {
		return false;
	}

	/** Returns the vertex color, white by default. */
	public Color vertexColor(V vertex) {
		return Color.WHITE;
	}

	/** Returns the list of vertices equivalent to the given one. All
	 * equivalent vertices will be shown as selected when on is selected. */
	public Set<V> equivalentVertices(V vertex) {
		return Collections.emptySet();
	}
}
