package plug.utils.ui.fx;

import javafx.application.Platform;
import javafx.beans.binding.StringBinding;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import org.kordamp.ikonli.javafx.FontIcon;
import plug.utils.file.FileWatcher;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GuiFxUtils {
    public static void addButtonEffects(Node btn) {
        //btn.setBackground(Background.EMPTY);

        DropShadow normalShadow = new DropShadow(3, Color.DARKGRAY);
        normalShadow.setBlurType(BlurType.GAUSSIAN);
        btn.setEffect(normalShadow);

        //set the default node shadow
        DropShadow hoverShadow = new DropShadow(3, Color.BLACK);
        hoverShadow.setBlurType(BlurType.GAUSSIAN);

        //install the node hover effect
        DropShadow clickShadow = new DropShadow(5, Color.GREEN);
        clickShadow.setBlurType(BlurType.GAUSSIAN);

        btn.addEventHandler(MouseEvent.MOUSE_ENTERED, e-> btn.setEffect(hoverShadow));
        btn.addEventHandler(MouseEvent.MOUSE_EXITED, e-> btn.setEffect(normalShadow));
        btn.addEventHandler(MouseEvent.MOUSE_PRESSED, e-> btn.setEffect(clickShadow));
        btn.addEventHandler(MouseEvent.MOUSE_RELEASED, e-> btn.setEffect(hoverShadow));
    }

    public static void setStylesheet(URL cssFilePath, Scene inScene, FileWatcher theWatcher) {

//        try {
            String external = cssFilePath.toExternalForm();
            inScene.getStylesheets().add(external);

//            theWatcher.addOnContentChange(cssFilePath, (c) -> Platform.runLater(() -> {
//                inScene.getStylesheets().clear();
//                inScene.getStylesheets().add(external);}
//            ));

//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
    }

    public static void addTooltip(StringBinding textBinding, Node shape) {
        Tooltip tooltip = new Tooltip();
        tooltip.textProperty().bind(textBinding);
        tooltip.setFont(Font.font("Times"));
        Tooltip.install(shape, tooltip);
    }
}
